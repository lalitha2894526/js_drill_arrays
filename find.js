function find(elements, callback) {
    try {
        if (!Array.isArray(elements)) {
            throw new TypeError("First argument must be an array");
        }
        if (typeof callback !== "function") {
            throw new TypeError("Second argument must be a function");
        }

        for (let index = 0; index < elements.length; index++) {
            if (callback(elements[index],index,elements)) {
                return elements[index];
            }
        }
        return undefined;
    } catch (error) {
        console.error(error.message);
        return undefined;
    }
}

module.exports=find;
