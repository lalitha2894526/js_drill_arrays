function filter(elements, callback) {
    try {
        if (!Array.isArray(elements)) {
            throw new TypeError("First argument must be an array");
        }
        if (typeof callback !== "function") {
            throw new TypeError("Second argument must be a function");
        }

        const result = [];
        for (let index = 0; index < elements.length; index++) {
            if (callback(elements[index],index,elements)) {
                result.push(elements[index]);
            }
        }
        return result;
    }
  catch (error) {
        console.error(error.message);
        return [];
    }
}

module.exports=filter;