
function reduce(elements, callback, startingValue) {
    try {
        if (!Array.isArray(elements)) {
            throw new TypeError("First argument must be an array");
        }
        if (typeof callback !== "function") {
            throw new TypeError("Second argument must be a function");
        }
        if (elements.length === 0 && startingValue === undefined) {
            throw new TypeError("Reduce of empty array with no initial value");
        }

        let accumulator;
        let startIndex;

        if (initialValue !== undefined) {
        accumulator = initialValue;
        startIndex = 0;
        }
         else {
        accumulator = array[0];
        startIndex = 1;
        }
        for (let index = startIndex; index < elements.length; index++) {
            accumulator = callback(accumulator, elements[index], index, elements);
        }
        return accumulator;
    } catch (error) {
        console.error(error.message);
        return undefined;
    }
}

module.exports = reduce;