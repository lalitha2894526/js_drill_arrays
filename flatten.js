function flatten(elements) {
    try {
        if (!Array.isArray(elements)) {
            throw new TypeError("First argument must be an array");
        }

        const result = [];
        function flattenHelper(array) {
            for (let index = 0; index < array.length; index++) {
                if (Array.isArray(array[index])) {
                    flattenHelper(array[index]);
                } else {
                    result.push(array[index]);
                }
            }
        }
        flattenHelper(elements);
        return result;
    } catch (error) {
        console.error(error.message);
        return [];
    }
}

module.exports=flatten;
