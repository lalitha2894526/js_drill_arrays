function map(elements,callback){
    try{
        if(!Array.isArray(elements)){
            throw new TypeError("First argumen must be array");
        }
        if(typeof callback !=="function"){
            throw new TypeError("Second argument must be a function");
        }

    }
    catch(error){
        console.log(error.message);
    }
    const result=[];
    for(let index=0;index<elements.length;index++){
        result.push(callback(elements[index],index,elements));
    }
    return result;
}
module.exports=map;