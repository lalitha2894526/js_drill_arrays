function each(elements, callback) {
    try {
        if (!Array.isArray(elements)) {
            throw new TypeError("First element must be array");
        }
        if (typeof callback !== "function") {
            throw new TypeError("Second argument must be a function");
        }
    }
    catch (error) {
        console.log(error.message);
    }
    for (let index = 0; index < elements.length; index++) {
        callback(elements[index], index,elements);
    }
}
module.exports = each;